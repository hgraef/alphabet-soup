package com.app;

//Hailey Graef
//March 2019

import java.util.Scanner;
import java.util.ArrayList;
import java.io.File;
import java.io.IOException;

public class AlphabetSoup {
    public static void main(String[] args){

        //imports file
        String fileName = "sourcefile";
        Scanner inFile = null;
        try{
            inFile = new Scanner(new File(fileName));
            if(!inFile.hasNext())
                throw new IOException();
        } catch (IOException ex){
            System.out.println("File "+fileName+" does not exist.");
        }

        String [][] puzzle = getPuzzle (inFile);
        ArrayList<String> words = getWords (inFile);
        boolean test;

        int wordIndex = 0; //counter to ensure all words are found

        while (wordIndex < words.size()) {
            ArrayList<Integer> firstLetterLocations = findFirstLetter(puzzle, words, wordIndex);//finds all occurrences of the first letter
            do{
                ArrayList<Integer> wordLocations = findNextLetter(puzzle, words, firstLetterLocations, wordIndex);//tests all letters around first and builds word if next letter is found
                test = wordTest(puzzle, words, wordLocations, wordIndex);//compares the word found with the word desired
                if (test) {
                    int count = 0;
                    String wordTest = "";
                    while (count < wordLocations.size()) {
                        wordTest += puzzle[wordLocations.get(count)][wordLocations.get(count+1)];
                        count+=2;
                    }
                    System.out.printf("The word " + wordTest + " was found at location [%d %d] [%d %d].\n", wordLocations.get(0), wordLocations.get(1),
                            wordLocations.get(wordLocations.size() - 2), wordLocations.get(wordLocations.size() - 1));//displays if word was found
                } else {
                    firstLetterLocations.remove(0);//deletes the location of the first coordinates if false and loops back to try next
                    firstLetterLocations.remove(0);//no error trapping as it's assumed you will find an occurrence of all words
                }
            }while(!test);
            wordIndex++;
        }
    }

    public static String[][] getPuzzle (Scanner inFile) {

       //gets crossword dimensions
        String grid = "";
        grid = inFile.nextLine();
        String[] tokens = grid.split("x");

        //puts crossword puzzle into a double array according to dimensions
        String [][] puzzle = new String[Integer.parseInt(tokens[0])][Integer.parseInt(tokens[1])];
        for (int i = 0; i < puzzle.length; i++) {
            String temp = inFile.nextLine();
            String[] tok = temp.split(" ");
            for (int j = 0; j < puzzle[i].length; j++) {
                puzzle[i][j] = tok[j];
            }
        }
        return puzzle;
    }

    public static ArrayList<String> getWords (Scanner inFile){
        //gets words and puts into ArrayList
        ArrayList<String> words = new ArrayList<>();
        while (inFile.hasNext()) {
            words.add(inFile.nextLine());
        }
        return words;
    }

    public static ArrayList<Integer> findFirstLetter (String [][] puzzle, ArrayList<String> words, int wordIndex) {

        ArrayList<Integer> firstLetterLocations = new ArrayList<>();

        //loops through entire 2-D array to find occurrences of first letter of word
        for (int i = 0; i < puzzle.length; i++) {
            for (int j = 0; j < puzzle[i].length; j++) {
                if ((puzzle[i][j]).charAt(0)==(words.get(wordIndex).charAt(0))){
                    firstLetterLocations.add(i);
                    firstLetterLocations.add(j);
                }
            }
        }
        return firstLetterLocations;
    }

    public static ArrayList<Integer> findNextLetter(String [][] puzzle, ArrayList<String> words, ArrayList<Integer> firstLetterLocations, int wordIndex) {

        //two Arrays to search in 8 different directions around the first location
        Integer[] searchRow = new Integer[]{-1, -1, -1, 0, 0, 1, 1, 1};
        Integer[] searchCol = new Integer[]{-1, 0, 1, -1, 1, -1, 0, 1};

        int row = firstLetterLocations.get(0);
        int col = firstLetterLocations.get(1);
        int letterIndex = 1;

        ArrayList<Integer> wordLocations = new ArrayList<>();
        wordLocations.add(row);
        wordLocations.add(col);

        for (int i = 0; i < searchCol.length; i++) {
            try {
                //finds second letter
                if ((puzzle[row + searchRow[i]][col + searchCol[i]]).charAt(0) == words.get(wordIndex).charAt(letterIndex)) {
                    //assumes the word moves in only one direction and grabs locations until length equals that of the word given
                    while (wordLocations.size() < (2 * (words.get(wordIndex).length()))) {
                        row = (row + searchRow[i]);
                        col = (col + searchCol[i]);
                        wordLocations.add(row);
                        wordLocations.add(col);
                    }
                }
              //catches exceptions and ignores them for edge letters
            } catch (ArrayIndexOutOfBoundsException a) {
            }
        }
        letterIndex++;
        return wordLocations;
    }

    public static boolean wordTest(String [][] puzzle, ArrayList<String> words, ArrayList<Integer> locations, int wordIndex) {
        //tests that word found matches word given
        int count = 0;
        String wordTest = "";
        while(count<locations.size()){
            wordTest+=puzzle[locations.get(count)][locations.get(count+1)];
            count+=2;
        }
        if (wordTest.equalsIgnoreCase(words.get(wordIndex)))
            return true;
        else
            return false;
    }

}
